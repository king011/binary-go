package core

import (
	"fmt"
	"math"
)

// MarshalSizeAdd .
func MarshalSizeAdd(ctx Context, use, add int) (size int, e error) {
	maxBuffer := ctx.MaxBuffer()
	if add > math.MaxUint16 {
		e = Error{
			Code:    CodeMaxBuffer,
			Message: fmt.Sprintf("max length<%v> not support, max length = <0xFFFF>", maxBuffer),
		}
		return
	} else if add > maxBuffer {
		e = Error{
			Code:    CodeMessageTooLong,
			Message: fmt.Sprintf("meaagse length<%v> to long, max length = <%v>", add, maxBuffer),
		}
		return
	}

	use += add
	if use > math.MaxUint16 {
		e = Error{
			Code:    CodeMaxBuffer,
			Message: fmt.Sprintf("max length<%v> not support, max length = <0xFFFF>", use),
		}
		return
	} else if use > maxBuffer {
		e = Error{
			Code:    CodeMessageTooLong,
			Message: fmt.Sprintf("meaagse length <%v> to long, max length = <%v>", use, maxBuffer),
		}
		return
	}

	size = use
	return
}

// MarshalInt16Size .
func MarshalInt16Size(ctx Context, use int, v int16) (int, error) {
	if v != 0 {
		return MarshalSizeAdd(ctx, use, 2+2)
	}
	return use, nil
}

// MarshalInt32Size .
func MarshalInt32Size(ctx Context, use int, v int32) (int, error) {
	if v != 0 {
		return MarshalSizeAdd(ctx, use, 2+4)
	}
	return use, nil
}

// MarshalInt64Size .
func MarshalInt64Size(ctx Context, use int, v int64) (int, error) {
	if v != 0 {
		return MarshalSizeAdd(ctx, use, 2+8)
	}
	return use, nil
}

// MarshalUint16Size .
func MarshalUint16Size(ctx Context, use int, v uint16) (int, error) {
	if v != 0 {
		return MarshalSizeAdd(ctx, use, 2+2)
	}
	return use, nil
}

// MarshalUint32Size .
func MarshalUint32Size(ctx Context, use int, v uint32) (int, error) {
	if v != 0 {
		return MarshalSizeAdd(ctx, use, 2+4)
	}
	return use, nil
}

// MarshalUint64Size .
func MarshalUint64Size(ctx Context, use int, v uint64) (int, error) {
	if v != 0 {
		return MarshalSizeAdd(ctx, use, 2+8)
	}
	return use, nil
}

// MarshalFloat32Size .
func MarshalFloat32Size(ctx Context, use int, v float32) (int, error) {
	if v != 0 {
		return MarshalSizeAdd(ctx, use, 2+4)
	}
	return use, nil
}

// MarshalFloat64Size .
func MarshalFloat64Size(ctx Context, use int, v float64) (int, error) {
	if v != 0 {
		return MarshalSizeAdd(ctx, use, 2+8)
	}
	return use, nil
}

// MarshalByteSize .
func MarshalByteSize(ctx Context, use int, v byte) (int, error) {
	if v != 0 {
		return MarshalSizeAdd(ctx, use, 2+1)
	}
	return use, nil
}

// MarshalBoolSize .
func MarshalBoolSize(ctx Context, use int, v bool) (int, error) {
	if v {
		return MarshalSizeAdd(ctx, use, 2+1)
	}
	return use, nil
}

// MarshalStringSize .
func MarshalStringSize(ctx Context, use int, v string) (int, error) {
	if len(v) != 0 {
		return MarshalSizeAdd(ctx, use, 2+2+len(v))
	}
	return use, nil
}

// MarshalInt16sSize .
func MarshalInt16sSize(ctx Context, use int, v []int16) (int, error) {
	if len(v) != 0 {
		return MarshalSizeAdd(ctx, use, 2+2+2*len(v))
	}
	return use, nil
}

// MarshalInt32sSize .
func MarshalInt32sSize(ctx Context, use int, v []int32) (int, error) {
	if len(v) != 0 {
		return MarshalSizeAdd(ctx, use, 2+2+4*len(v))
	}
	return use, nil
}

// MarshalInt64sSize .
func MarshalInt64sSize(ctx Context, use int, v []int64) (int, error) {
	if len(v) != 0 {
		return MarshalSizeAdd(ctx, use, 2+2+8*len(v))
	}
	return use, nil
}

// MarshalUint16sSize .
func MarshalUint16sSize(ctx Context, use int, v []uint16) (int, error) {
	if len(v) != 0 {
		return MarshalSizeAdd(ctx, use, 2+2+2*len(v))
	}
	return use, nil
}

// MarshalUint32sSize .
func MarshalUint32sSize(ctx Context, use int, v []uint32) (int, error) {
	if len(v) != 0 {
		return MarshalSizeAdd(ctx, use, 2+2+4*len(v))
	}
	return use, nil
}

// MarshalUint64sSize .
func MarshalUint64sSize(ctx Context, use int, v []uint64) (int, error) {
	if len(v) != 0 {
		return MarshalSizeAdd(ctx, use, 2+2+8*len(v))
	}
	return use, nil
}

// MarshalFloat32sSize .
func MarshalFloat32sSize(ctx Context, use int, v []float32) (int, error) {
	if len(v) != 0 {
		return MarshalSizeAdd(ctx, use, 2+2+4*len(v))
	}
	return use, nil
}

// MarshalFloat64sSize .
func MarshalFloat64sSize(ctx Context, use int, v []float64) (int, error) {
	if len(v) != 0 {
		return MarshalSizeAdd(ctx, use, 2+2+8*len(v))
	}
	return use, nil
}

// MarshalBytesSize .
func MarshalBytesSize(ctx Context, use int, v []byte) (int, error) {
	if len(v) != 0 {
		return MarshalSizeAdd(ctx, use, 2+2+len(v))
	}
	return use, nil
}

// MarshalBoolsSize .
func MarshalBoolsSize(ctx Context, use int, v []bool) (int, error) {
	if len(v) != 0 {
		if len(v) > math.MaxUint16 {
			return 0, Error{
				Code:    CodeMaxBuffer,
				Message: fmt.Sprintf("bits length<%v> not support, max length = 0xFFFF", len(v)),
			}
		}
		return MarshalSizeAdd(ctx, use, 2+2+(len(v)+7)/8)
	}
	return use, nil
}

// MarshalStringsSize .
func MarshalStringsSize(ctx Context, use int, v []string) (int, error) {
	count := len(v)
	if count != 0 {
		var e error
		use, e = MarshalSizeAdd(ctx, use, 2+2)
		if e != nil {
			return 0, e
		}
		for i := 0; i < count; i++ {
			use, e = MarshalSizeAdd(ctx, use, 2+len(v[i]))
			if e != nil {
				return 0, e
			}
		}
	}
	return use, nil
}

// MarshalTypeSize .
func MarshalTypeSize(ctx Context, use int, v Type) (size int, e error) {
	use, e = MarshalSizeAdd(ctx, use, 2+2)
	if e != nil {
		return
	}
	use, e = v.MarshalSize(ctx, use)
	if e != nil {
		return
	}
	size = use
	return
}
