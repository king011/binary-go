package core

import (
	"encoding/binary"
	"fmt"
	"math"
)

// PutID 設置 id
func PutID(order binary.ByteOrder, b []byte, id uint16, tag uint16) {
	order.PutUint16(b, id|(tag<<13))
}

// MarshalAdd 增加緩衝區使用長度
func MarshalAdd(ctx Context, use, add, capacity int) (size int, e error) {
	maxBuffer := ctx.MaxBuffer()
	if maxBuffer > math.MaxUint16 {
		e = Error{
			Code:    CodeMaxBuffer,
			Message: fmt.Sprintf("max length<%v> not support, max length = 0xFFFF", maxBuffer),
		}
		return
	} else if add > maxBuffer {
		e = Error{
			Code:    CodeMessageTooLong,
			Message: fmt.Sprintf("meaagse length<%v> to long, max length = <%v>", add, maxBuffer),
		}
		return
	} else if add > capacity {
		e = Error{
			Code:    CodeBufferTooSmall,
			Message: fmt.Sprintf("buffer length<%v> too small", capacity),
		}
		return
	}

	use += add
	if use > maxBuffer {
		e = Error{
			Code:    CodeMessageTooLong,
			Message: fmt.Sprintf("meaagse length<%v> to long, max length = <%v>", use, maxBuffer),
		}
		return
	} else if use > capacity {
		e = Error{
			Code:    CodeBufferTooSmall,
			Message: fmt.Sprintf("buffer length<%v> too small", capacity),
		}
		return
	}

	size = use
	return
}

// MarshalInt16 .
func MarshalInt16(ctx Context, buffer []byte, use int, id uint16, v int16) (size int, e error) {
	if v == 0 {
		size = use
		return
	}
	capacity := len(buffer)
	buffer = buffer[use:]
	use, e = MarshalAdd(ctx, use, 2+2, capacity)
	if e != nil {
		return
	}

	order := ctx.ByteOrder()
	PutID(order, buffer, id, BinaryWrite16)
	order.PutUint16(buffer[2:], uint16(v))
	size = use
	return
}

// MarshalInt32 .
func MarshalInt32(ctx Context, buffer []byte, use int, id uint16, v int32) (size int, e error) {
	if v == 0 {
		size = use
		return
	}
	capacity := len(buffer)
	buffer = buffer[use:]
	use, e = MarshalAdd(ctx, use, 2+4, capacity)
	if e != nil {
		return
	}

	order := ctx.ByteOrder()
	PutID(order, buffer, id, BinaryWrite32)
	order.PutUint32(buffer[2:], uint32(v))
	size = use
	return
}

// MarshalInt64 .
func MarshalInt64(ctx Context, buffer []byte, use int, id uint16, v int64) (size int, e error) {
	if v == 0 {
		size = use
		return
	}
	capacity := len(buffer)
	buffer = buffer[use:]
	use, e = MarshalAdd(ctx, use, 2+8, capacity)
	if e != nil {
		return
	}

	order := ctx.ByteOrder()
	PutID(order, buffer, id, BinaryWrite64)
	order.PutUint64(buffer[2:], uint64(v))
	size = use
	return
}

// MarshalUint16 .
func MarshalUint16(ctx Context, buffer []byte, use int, id uint16, v uint16) (size int, e error) {
	if v == 0 {
		size = use
		return
	}
	capacity := len(buffer)
	buffer = buffer[use:]
	use, e = MarshalAdd(ctx, use, 2+2, capacity)
	if e != nil {
		return
	}

	order := ctx.ByteOrder()
	PutID(order, buffer, id, BinaryWrite16)
	order.PutUint16(buffer[2:], v)
	size = use
	return
}

// MarshalUint32 .
func MarshalUint32(ctx Context, buffer []byte, use int, id uint16, v uint32) (size int, e error) {
	if v == 0 {
		size = use
		return
	}
	capacity := len(buffer)
	buffer = buffer[use:]
	use, e = MarshalAdd(ctx, use, 2+4, capacity)
	if e != nil {
		return
	}

	order := ctx.ByteOrder()
	PutID(order, buffer, id, BinaryWrite32)
	order.PutUint32(buffer[2:], v)
	size = use
	return
}

// MarshalUint64 .
func MarshalUint64(ctx Context, buffer []byte, use int, id uint16, v uint64) (size int, e error) {
	if v == 0 {
		size = use
		return
	}
	capacity := len(buffer)
	buffer = buffer[use:]
	use, e = MarshalAdd(ctx, use, 2+8, capacity)
	if e != nil {
		return
	}

	order := ctx.ByteOrder()
	PutID(order, buffer, id, BinaryWrite64)
	order.PutUint64(buffer[2:], v)
	size = use
	return
}

// MarshalFloat32 .
func MarshalFloat32(ctx Context, buffer []byte, use int, id uint16, v float32) (size int, e error) {
	if v == 0 {
		size = use
		return
	}
	capacity := len(buffer)
	buffer = buffer[use:]
	use, e = MarshalAdd(ctx, use, 2+4, capacity)
	if e != nil {
		return
	}

	order := ctx.ByteOrder()
	PutID(order, buffer, id, BinaryWrite32)
	order.PutUint32(buffer[2:], math.Float32bits(v))
	size = use
	return
}

// MarshalFloat64 .
func MarshalFloat64(ctx Context, buffer []byte, use int, id uint16, v float64) (size int, e error) {
	if v == 0 {
		size = use
		return
	}
	capacity := len(buffer)
	buffer = buffer[use:]
	use, e = MarshalAdd(ctx, use, 2+8, capacity)
	if e != nil {
		return
	}

	order := ctx.ByteOrder()
	PutID(order, buffer, id, BinaryWrite64)
	order.PutUint64(buffer[2:], math.Float64bits(v))
	size = use
	return
}

// MarshalByte .
func MarshalByte(ctx Context, buffer []byte, use int, id uint16, v byte) (size int, e error) {
	if v == 0 {
		size = use
		return
	}
	capacity := len(buffer)
	buffer = buffer[use:]
	use, e = MarshalAdd(ctx, use, 2+1, capacity)
	if e != nil {
		return
	}

	order := ctx.ByteOrder()
	PutID(order, buffer, id, BinaryWrite8)
	buffer[2] = v
	size = use
	return
}

// MarshalBool .
func MarshalBool(ctx Context, buffer []byte, use int, id uint16, v bool) (size int, e error) {
	if !v {
		size = use
		return
	}
	capacity := len(buffer)
	buffer = buffer[use:]
	use, e = MarshalAdd(ctx, use, 2+1, capacity)
	if e != nil {
		return
	}

	order := ctx.ByteOrder()
	PutID(order, buffer, id, BinaryWrite8)
	buffer[2] = 1
	size = use
	return
}

// MarshalString .
func MarshalString(ctx Context, buffer []byte, use int, id uint16, v string) (size int, e error) {
	if len(v) == 0 {
		size = use
		return
	}
	capacity := len(buffer)
	buffer = buffer[use:]
	use, e = MarshalAdd(ctx, use, 4+len(v), capacity)
	if e != nil {
		return
	}

	order := ctx.ByteOrder()
	PutID(order, buffer, id, BinaryWriteLength)
	order.PutUint16(buffer[2:], uint16(len(v)))
	copy(buffer[4:], v)
	size = use
	return
}

// MarshalInt16s .
func MarshalInt16s(ctx Context, buffer []byte, use int, id uint16, arrs []int16) (size int, e error) {
	if len(arrs) == 0 {
		size = use
		return
	}
	capacity := len(buffer)
	buffer = buffer[use:]
	valSize := 2
	arrsSize := len(arrs) * valSize
	use, e = MarshalAdd(ctx, use, 4+arrsSize, capacity)
	if e != nil {
		return
	}

	order := ctx.ByteOrder()
	PutID(order, buffer, id, BinaryWriteLength)
	order.PutUint16(buffer[2:], uint16(arrsSize))
	buffer = buffer[4:]
	for i := 0; i < len(arrs); i++ {
		order.PutUint16(buffer, uint16(arrs[i]))
		buffer = buffer[valSize:]
	}
	size = use
	return
}

// MarshalInt32s .
func MarshalInt32s(ctx Context, buffer []byte, use int, id uint16, arrs []int32) (size int, e error) {
	if len(arrs) == 0 {
		size = use
		return
	}
	capacity := len(buffer)
	buffer = buffer[use:]
	valSize := 4
	arrsSize := len(arrs) * valSize
	use, e = MarshalAdd(ctx, use, 4+arrsSize, capacity)
	if e != nil {
		return
	}

	order := ctx.ByteOrder()
	PutID(order, buffer, id, BinaryWriteLength)
	order.PutUint16(buffer[2:], uint16(arrsSize))
	buffer = buffer[4:]
	for i := 0; i < len(arrs); i++ {
		order.PutUint32(buffer, uint32(arrs[i]))
		buffer = buffer[valSize:]
	}
	size = use
	return
}

// MarshalInt64s .
func MarshalInt64s(ctx Context, buffer []byte, use int, id uint16, arrs []int64) (size int, e error) {
	if len(arrs) == 0 {
		size = use
		return
	}
	capacity := len(buffer)
	buffer = buffer[use:]
	valSize := 8
	arrsSize := len(arrs) * valSize
	use, e = MarshalAdd(ctx, use, 4+arrsSize, capacity)
	if e != nil {
		return
	}

	order := ctx.ByteOrder()
	PutID(order, buffer, id, BinaryWriteLength)
	order.PutUint16(buffer[2:], uint16(arrsSize))
	buffer = buffer[4:]
	for i := 0; i < len(arrs); i++ {
		order.PutUint64(buffer, uint64(arrs[i]))
		buffer = buffer[valSize:]
	}
	size = use
	return
}

// MarshalUint16s .
func MarshalUint16s(ctx Context, buffer []byte, use int, id uint16, arrs []uint16) (size int, e error) {
	if len(arrs) == 0 {
		size = use
		return
	}
	capacity := len(buffer)
	buffer = buffer[use:]
	valSize := 2
	arrsSize := len(arrs) * valSize
	use, e = MarshalAdd(ctx, use, 4+arrsSize, capacity)
	if e != nil {
		return
	}

	order := ctx.ByteOrder()
	PutID(order, buffer, id, BinaryWriteLength)
	order.PutUint16(buffer[2:], uint16(arrsSize))
	buffer = buffer[4:]
	for i := 0; i < len(arrs); i++ {
		order.PutUint16(buffer, arrs[i])
		buffer = buffer[valSize:]
	}
	size = use
	return
}

// MarshalUint32s .
func MarshalUint32s(ctx Context, buffer []byte, use int, id uint16, arrs []uint32) (size int, e error) {
	if len(arrs) == 0 {
		size = use
		return
	}
	capacity := len(buffer)
	buffer = buffer[use:]
	valSize := 4
	arrsSize := len(arrs) * valSize
	use, e = MarshalAdd(ctx, use, 4+arrsSize, capacity)
	if e != nil {
		return
	}

	order := ctx.ByteOrder()
	PutID(order, buffer, id, BinaryWriteLength)
	order.PutUint16(buffer[2:], uint16(arrsSize))
	buffer = buffer[4:]
	for i := 0; i < len(arrs); i++ {
		order.PutUint32(buffer, arrs[i])
		buffer = buffer[valSize:]
	}
	size = use
	return
}

// MarshalUint64s .
func MarshalUint64s(ctx Context, buffer []byte, use int, id uint16, arrs []uint64) (size int, e error) {
	if len(arrs) == 0 {
		size = use
		return
	}
	capacity := len(buffer)
	buffer = buffer[use:]
	valSize := 8
	arrsSize := len(arrs) * valSize
	use, e = MarshalAdd(ctx, use, 4+arrsSize, capacity)
	if e != nil {
		return
	}

	order := ctx.ByteOrder()
	PutID(order, buffer, id, BinaryWriteLength)
	order.PutUint16(buffer[2:], uint16(arrsSize))
	buffer = buffer[4:]
	for i := 0; i < len(arrs); i++ {
		order.PutUint64(buffer, arrs[i])
		buffer = buffer[valSize:]
	}
	size = use
	return
}

// MarshalFloat32s .
func MarshalFloat32s(ctx Context, buffer []byte, use int, id uint16, arrs []float32) (size int, e error) {
	if len(arrs) == 0 {
		size = use
		return
	}
	capacity := len(buffer)
	buffer = buffer[use:]
	valSize := 4
	arrsSize := len(arrs) * valSize
	use, e = MarshalAdd(ctx, use, 4+arrsSize, capacity)
	if e != nil {
		return
	}

	order := ctx.ByteOrder()
	PutID(order, buffer, id, BinaryWriteLength)
	order.PutUint16(buffer[2:], uint16(arrsSize))
	buffer = buffer[4:]
	for i := 0; i < len(arrs); i++ {
		order.PutUint32(buffer, math.Float32bits(arrs[i]))
		buffer = buffer[valSize:]
	}
	size = use
	return
}

// MarshalFloat64s .
func MarshalFloat64s(ctx Context, buffer []byte, use int, id uint16, arrs []float64) (size int, e error) {
	if len(arrs) == 0 {
		size = use
		return
	}
	capacity := len(buffer)
	buffer = buffer[use:]
	valSize := 8
	arrsSize := len(arrs) * valSize
	use, e = MarshalAdd(ctx, use, 4+arrsSize, capacity)
	if e != nil {
		return
	}

	order := ctx.ByteOrder()
	PutID(order, buffer, id, BinaryWriteLength)
	order.PutUint16(buffer[2:], uint16(arrsSize))
	buffer = buffer[4:]
	for i := 0; i < len(arrs); i++ {
		order.PutUint64(buffer, math.Float64bits(arrs[i]))
		buffer = buffer[valSize:]
	}
	size = use
	return
}

// MarshalBytes .
func MarshalBytes(ctx Context, buffer []byte, use int, id uint16, arrs []byte) (size int, e error) {
	if len(arrs) == 0 {
		size = use
		return
	}
	capacity := len(buffer)
	buffer = buffer[use:]
	arrsSize := len(arrs)
	use, e = MarshalAdd(ctx, use, 4+arrsSize, capacity)
	if e != nil {
		return
	}

	order := ctx.ByteOrder()
	PutID(order, buffer, id, BinaryWriteLength)
	order.PutUint16(buffer[2:], uint16(arrsSize))
	copy(buffer[4:], arrs)
	size = use
	return
}

// MarshalBools .
func MarshalBools(ctx Context, buffer []byte, use int, id uint16, arrs []bool) (size int, e error) {
	if len(arrs) == 0 {
		size = use
		return
	}
	capacity := len(buffer)
	buffer = buffer[use:]
	arrsSize, e := MarshalBoolsSize(ctx, 0, arrs)
	if e != nil {
		return
	}
	use, e = MarshalAdd(ctx, use, arrsSize, capacity)
	if e != nil {
		return
	}

	order := ctx.ByteOrder()
	PutID(order, buffer, id, BinaryWriteBits)
	order.PutUint16(buffer[2:], uint16(len(arrs)))
	buffer = buffer[4:]
	for i, ok := range arrs {
		if ok {
			BitSet(&buffer[i/8], i%8, true)
		}
	}
	size = use
	return
}

// MarshalStrings .
func MarshalStrings(ctx Context, buffer []byte, use int, id uint16, strs []string) (size int, e error) {
	if len(strs) == 0 {
		size = use
		return
	}
	capacity := len(buffer)
	buffer = buffer[use:]
	arrsSize, e := MarshalStringsSize(ctx, 0, strs)
	if e != nil {
		return
	}
	use, e = MarshalAdd(ctx, use, arrsSize, capacity)
	if e != nil {
		return
	}

	order := ctx.ByteOrder()
	PutID(order, buffer, id, BinaryWriteLength)
	order.PutUint16(buffer[2:], uint16(arrsSize-4))
	buffer = buffer[4:]
	var strLen int
	for _, str := range strs {
		strLen = len(str)
		order.PutUint16(buffer, uint16(strLen))
		if strLen == 0 {
			buffer = buffer[2:]
		} else {
			copy(buffer[2:], str)
			buffer = buffer[2+strLen:]
		}
	}

	size = use
	return
}
