package core

// MaxFieldID 最大 id
const MaxFieldID = 0x1FFF

// DefaultMaxBuffer 默認最大編碼長度
const DefaultMaxBuffer = 1024 * 50

const (
	// BinaryWrite8 byte bool
	BinaryWrite8 = 1
	// BinaryWrite16 int16 uint16 enum
	BinaryWrite16 = 2
	// BinaryWrite32 float32 int32 uint32
	BinaryWrite32 = 3
	// BinaryWrite64 float64 int64 uint64
	BinaryWrite64 = 4
	// BinaryWriteLength bytes strings type repeat
	BinaryWriteLength = 5
	// BinaryWriteBits bit repeat
	BinaryWriteBits = 6
)
