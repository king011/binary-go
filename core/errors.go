package core

import "fmt"

const (
	// CodeRange 訪問越界
	CodeRange = -11
	// CodeMaxBuffer 編碼長度 超過 uint16 限定
	CodeMaxBuffer = -20

	// CodeMessageTooLong 消息超過最大長度
	CodeMessageTooLong = -100

	// CodeBufferTooSmall 緩衝區太小
	CodeBufferTooSmall = -201

	// CodeUnmarshalField Field 無法解開
	CodeUnmarshalField = -300
	// CodeUnmarshalFieldUnknowWriteType 未知 field 寫入類型
	CodeUnmarshalFieldUnknowWriteType = -301
	// CodeUnmarshalFieldLength Field 長度不匹配
	CodeUnmarshalFieldLength = -302
)

// Error 錯誤定義
type Error struct {
	Code    int
	Message string
}

func (e Error) Error() (str string) {
	switch e.Code {
	case CodeMessageTooLong:
		str = fmt.Sprintf("{ message too long <%d> , %s }", e.Code, e.Message)
	default:
		str = fmt.Sprintf("{ unknow <%d> , %s }", e.Code, e.Message)
	}
	return
}
