package core

import (
	"encoding/binary"
)

// Context 編碼環境定義
type Context interface {
	// MaxBuffer 返回 支持的 最大編碼 長度
	MaxBuffer() int

	// ByteOrder 返回 被
	ByteOrder() binary.ByteOrder
}

// Type 類型接口
type Type interface {
	// MarshalSize 返回 編碼需要緩衝區大小
	MarshalSize(ctx Context, use int) (size int, e error)
	// MarshalTo 編碼到二進制
	Marshal(ctx Context) (buffer []byte, e error)
	// MarshalTo 編碼到二進制
	MarshalToBuffer(ctx Context, buffer []byte, use int) (size int, e error)
	// Unmarshal 由二進制解碼到內存結構
	Unmarshal(ctx Context, buffer []byte) (e error)
	// Reset 重置 所有屬性到 初始值
	Reset()
}
