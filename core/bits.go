package core

import "fmt"

// BitSet 設置 bit
func BitSet(val *byte, i int, ok bool) {
	switch i {
	case 0:
		if ok {
			*val |= 1
		} else {
			*val &= ^byte(1)
		}
	case 1:
		if ok {
			*val |= 2
		} else {
			*val &= ^byte(2)
		}
	case 2:
		if ok {
			*val |= 4
		} else {
			*val &= ^byte(4)
		}
	case 3:
		if ok {
			*val |= 8
		} else {
			*val &= ^byte(8)
		}
	case 4:
		if ok {
			*val |= 16
		} else {
			*val &= ^byte(16)
		}
	case 5:
		if ok {
			*val |= 32
		} else {
			*val &= ^byte(32)
		}
	case 6:
		if ok {
			*val |= 64
		} else {
			*val &= ^byte(64)
		}
	case 7:
		if ok {
			*val |= 128
		} else {
			*val &= ^byte(128)
		}
	default:
		e := Error{
			Code:    CodeRange,
			Message: fmt.Sprintf("BitSet not support index<%v>", i),
		}
		panic(e)
	}
	return
}

// BitGet 返回 bit 是否被設置
func BitGet(val byte, i int) (ok bool) {
	switch i {
	case 0:
		val &= 1
	case 1:
		val &= 2
	case 2:
		val &= 4
	case 3:
		val &= 8
	case 4:
		val &= 16
	case 5:
		val &= 32
	case 6:
		val &= 64
	case 7:
		val &= 128
	default:
		e := Error{
			Code:    CodeRange,
			Message: fmt.Sprintf("BitGet not support index<%v>", i),
		}
		panic(e)
	}
	return val != 0
}
