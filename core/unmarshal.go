package core

import (
	"encoding/binary"
	"fmt"
	"math"
)

// Field 編碼後的 屬性信息
type Field struct {
	ID     uint16
	Buffer []byte
	// 僅僅 bis 有效 指定 bis 數組長度
	Len int
}

func unmarshalField(order binary.ByteOrder, buffer []byte, field *Field) ([]byte, error) {
	if len(buffer) < 2 {
		return nil, Error{
			Code:    CodeUnmarshalField,
			Message: fmt.Sprintf("unmarshal field tag, len(buffer)<%v> < 2", len(buffer)),
		}
	}
	field.ID = order.Uint16(buffer)
	wt := field.ID >> 13
	switch wt {
	case BinaryWrite8:
		if len(buffer) < 2+1 {
			return nil, Error{
				Code:    CodeUnmarshalField,
				Message: fmt.Sprintf("unmarshal field, write 8, len(buffer)<%v> < 2+1", len(buffer)),
			}
		}
		field.Buffer = buffer[2 : 2+1]
		buffer = buffer[2+1:]
	case BinaryWrite16:
		if len(buffer) < 2+2 {
			return nil, Error{
				Code:    CodeUnmarshalField,
				Message: fmt.Sprintf("unmarshal field, write 16, len(buffer)<%v> < 2+2", len(buffer)),
			}
		}
		field.Buffer = buffer[2 : 2+2]
		buffer = buffer[2+2:]
	case BinaryWrite32:
		if len(buffer) < 2+4 {
			return nil, Error{
				Code:    CodeUnmarshalField,
				Message: fmt.Sprintf("unmarshal field, write 32, len(buffer)<%v> < 2+4", len(buffer)),
			}
		}
		field.Buffer = buffer[2 : 2+4]
		buffer = buffer[2+4:]
	case BinaryWrite64:
		if len(buffer) < 2+8 {
			return nil, Error{
				Code:    CodeUnmarshalField,
				Message: fmt.Sprintf("unmarshal field, write 64, len(buffer)<%v> < 2+8", len(buffer)),
			}
		}
		field.Buffer = buffer[2 : 2+8]
		buffer = buffer[2+8:]
	case BinaryWriteLength:
		if len(buffer) < 2+2 {
			return nil, Error{
				Code:    CodeUnmarshalField,
				Message: fmt.Sprintf("unmarshal field, write length, len(buffer)<%v> < 2+2", len(buffer)),
			}
		}
		size := (int)(order.Uint16(buffer[2:]))
		if size == 0 {
			buffer = buffer[2+2:]
		} else {
			if len(buffer) < 2+2+size {
				return nil, Error{
					Code:    CodeUnmarshalFieldLength,
					Message: fmt.Sprintf("unmarshal field, write length, len(buffer)<%v> < 2+2+%v", len(buffer), size),
				}
			}
			field.Buffer = buffer[2+2 : 2+2+size]
			buffer = buffer[2+2+size:]
		}
	case BinaryWriteBits:
		if len(buffer) < 2+2 {
			return nil, Error{
				Code:    CodeUnmarshalField,
				Message: fmt.Sprintf("unmarshal field, write bits, len(buffer)<%v> < 2+2", len(buffer)),
			}
		}
		field.Len = (int)(order.Uint16(buffer[2:]))
		size := (field.Len + 7) / 8
		if size == 0 {
			buffer = buffer[2+2:]
		} else {
			if len(buffer) < 2+2+size {
				return nil, Error{
					Code:    CodeUnmarshalFieldLength,
					Message: fmt.Sprintf("unmarshal field, write bits, len(buffer)<%v> < 2+2+%v", len(buffer), size),
				}
			}
			field.Buffer = buffer[2+2 : 2+2+size]
			buffer = buffer[2+2+size:]
		}
	default:
		return nil, Error{
			Code:    CodeUnmarshalFieldUnknowWriteType,
			Message: fmt.Sprintf("unmarshal field, unknow write type<%v>", wt),
		}
	}
	field.ID &= 0x1FFF
	return buffer, nil
}

// UnmarshalFields 返回 編碼的 Field 信息
func UnmarshalFields(ctx Context, buffer []byte, fields []Field) (e error) {
	count := len(fields)
	var i int
	order := ctx.ByteOrder()
	for i < count && len(buffer) != 0 {
		var field Field
		buffer, e = unmarshalField(order, buffer, &field)
		if e != nil {
			return
		}
		if field.ID < fields[i].ID {
			continue
		}
		for ; i < count; i++ {
			if field.ID == fields[i].ID {
				fields[i].Buffer = field.Buffer
				fields[i].Len = field.Len
				i++
				break
			} else if field.ID < fields[i].ID {
				break
			}
		}
	}
	return
}

// UnmarshalInt16 .
func UnmarshalInt16(ctx Context, field Field) (v int16) {
	if len(field.Buffer) == 0 {
		return
	}
	order := ctx.ByteOrder()
	v = int16(order.Uint16(field.Buffer))
	return
}

// UnmarshalInt32 .
func UnmarshalInt32(ctx Context, field Field) (v int32) {
	if len(field.Buffer) == 0 {
		return
	}
	order := ctx.ByteOrder()
	v = int32(order.Uint32(field.Buffer))
	return
}

// UnmarshalInt64 .
func UnmarshalInt64(ctx Context, field Field) (v int64) {
	if len(field.Buffer) == 0 {
		return
	}
	order := ctx.ByteOrder()
	v = int64(order.Uint64(field.Buffer))
	return
}

// UnmarshalUint16 .
func UnmarshalUint16(ctx Context, field Field) (v uint16) {
	if len(field.Buffer) == 0 {
		return
	}
	order := ctx.ByteOrder()
	v = order.Uint16(field.Buffer)
	return
}

// UnmarshalUint32 .
func UnmarshalUint32(ctx Context, field Field) (v uint32) {
	if len(field.Buffer) == 0 {
		return
	}
	order := ctx.ByteOrder()
	v = order.Uint32(field.Buffer)
	return
}

// UnmarshalUint64 .
func UnmarshalUint64(ctx Context, field Field) (v uint64) {
	if len(field.Buffer) == 0 {
		return
	}
	order := ctx.ByteOrder()
	v = order.Uint64(field.Buffer)
	return
}

// UnmarshalFloat32 .
func UnmarshalFloat32(ctx Context, field Field) (v float32) {
	if len(field.Buffer) == 0 {
		return
	}
	order := ctx.ByteOrder()
	v = math.Float32frombits(order.Uint32(field.Buffer))
	return
}

// UnmarshalFloat64 .
func UnmarshalFloat64(ctx Context, field Field) (v float64) {
	if len(field.Buffer) == 0 {
		return
	}
	order := ctx.ByteOrder()
	v = math.Float64frombits(order.Uint64(field.Buffer))
	return
}

// UnmarshalByte .
func UnmarshalByte(ctx Context, field Field) (v byte) {
	if len(field.Buffer) == 0 {
		return
	}
	v = field.Buffer[0]
	return
}

// UnmarshalBool .
func UnmarshalBool(ctx Context, field Field) (v bool) {
	if len(field.Buffer) == 0 {
		return
	}
	if field.Buffer[0] != 0 {
		v = true
	}
	return
}

// UnmarshalString .
func UnmarshalString(ctx Context, field Field) (v string) {
	if len(field.Buffer) == 0 {
		return
	}
	v = string(field.Buffer)
	return
}

// UnmarshalInt16s .
func UnmarshalInt16s(ctx Context, field Field) (arrs []int16) {
	count := len(field.Buffer)
	if count == 0 {
		return
	}
	count /= 2
	order := ctx.ByteOrder()
	arrs = make([]int16, count)
	for i := 0; i < count; i++ {
		arrs[i] = int16(order.Uint16(field.Buffer[i*2:]))
	}
	return
}

// UnmarshalInt32s .
func UnmarshalInt32s(ctx Context, field Field) (arrs []int32) {
	count := len(field.Buffer)
	if count == 0 {
		return
	}
	count /= 4
	order := ctx.ByteOrder()
	arrs = make([]int32, count)
	for i := 0; i < count; i++ {
		arrs[i] = int32(order.Uint32(field.Buffer[i*4:]))
	}
	return
}

// UnmarshalInt64s .
func UnmarshalInt64s(ctx Context, field Field) (arrs []int64) {
	count := len(field.Buffer)
	if count == 0 {
		return
	}
	count /= 8
	order := ctx.ByteOrder()
	arrs = make([]int64, count)
	for i := 0; i < count; i++ {
		arrs[i] = int64(order.Uint64(field.Buffer[i*8:]))
	}
	return
}

// UnmarshalUint16s .
func UnmarshalUint16s(ctx Context, field Field) (arrs []uint16) {
	count := len(field.Buffer)
	if count == 0 {
		return
	}
	count /= 2
	order := ctx.ByteOrder()
	arrs = make([]uint16, count)
	for i := 0; i < count; i++ {
		arrs[i] = order.Uint16(field.Buffer[i*2:])
	}
	return
}

// UnmarshalUint32s .
func UnmarshalUint32s(ctx Context, field Field) (arrs []uint32) {
	count := len(field.Buffer)
	if count == 0 {
		return
	}
	count /= 4
	order := ctx.ByteOrder()
	arrs = make([]uint32, count)
	for i := 0; i < count; i++ {
		arrs[i] = order.Uint32(field.Buffer[i*4:])
	}
	return
}

// UnmarshalUint64s .
func UnmarshalUint64s(ctx Context, field Field) (arrs []uint64) {
	count := len(field.Buffer)
	if count == 0 {
		return
	}
	count /= 8
	order := ctx.ByteOrder()
	arrs = make([]uint64, count)
	for i := 0; i < count; i++ {
		arrs[i] = order.Uint64(field.Buffer[i*8:])
	}
	return
}

// UnmarshalFloat32s .
func UnmarshalFloat32s(ctx Context, field Field) (arrs []float32) {
	count := len(field.Buffer)
	if count == 0 {
		return
	}
	count /= 4
	order := ctx.ByteOrder()
	arrs = make([]float32, count)
	for i := 0; i < count; i++ {
		arrs[i] = math.Float32frombits(order.Uint32(field.Buffer[i*4:]))
	}
	return
}

// UnmarshalFloat64s .
func UnmarshalFloat64s(ctx Context, field Field) (arrs []float64) {
	count := len(field.Buffer)
	if count == 0 {
		return
	}
	count /= 8
	order := ctx.ByteOrder()
	arrs = make([]float64, count)
	for i := 0; i < count; i++ {
		arrs[i] = math.Float64frombits(order.Uint64(field.Buffer[i*8:]))
	}
	return
}

// UnmarshalBytes .
func UnmarshalBytes(ctx Context, field Field) (arrs []byte) {
	count := len(field.Buffer)
	if count == 0 {
		return
	}
	arrs = make([]byte, count)
	copy(arrs, field.Buffer)
	return
}

// UnmarshalBools .
func UnmarshalBools(ctx Context, field Field) (arrs []bool) {
	count := field.Len
	if count == 0 {
		return
	}
	arrs = make([]bool, count)
	for i := 0; i < count; i++ {
		arrs[i] = BitGet(field.Buffer[i/8], i%8)
	}
	return
}

// UnmarshalCount .
func UnmarshalCount(ctx Context, buffer []byte) (arrs [][]byte, e error) {
	if len(buffer) == 0 {
		return
	}
	order := ctx.ByteOrder()
	for len(buffer) != 0 {
		if len(buffer) < 2 {
			e = Error{
				Code:    CodeUnmarshalFieldLength,
				Message: fmt.Sprintf("unmarshal count,len(buffer)<%v> < 2", len(buffer)),
			}
			return
		}
		size := int(order.Uint16(buffer))
		if size == 0 {
			buffer = buffer[2:]
			arrs = append(arrs, nil)
		} else {
			if len(buffer) < 2+size {
				e = Error{
					Code:    CodeUnmarshalFieldLength,
					Message: fmt.Sprintf("unmarshal count,len(buffer)<%v> < 2+size<%v>", len(buffer), size),
				}
				return
			}
			arrs = append(arrs, buffer[2:2+size])
			buffer = buffer[2+size:]
		}
	}
	return
}

// UnmarshalStrings .
func UnmarshalStrings(ctx Context, field Field) (arrs []string, e error) {
	items, e := UnmarshalCount(ctx, field.Buffer)
	if e != nil || len(items) == 0 {
		return
	}
	arrs = make([]string, len(items))
	for i, item := range items {
		if len(item) != 0 {
			arrs[i] = string(item)
		}
	}
	return
}
