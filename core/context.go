package core

import "encoding/binary"

type _Context struct {
}

var _Background _Context

// Background 返回 默認 編碼 環境
func Background() Context {
	return _Background
}
func (_Context) MaxBuffer() int {
	return DefaultMaxBuffer
}
func (_Context) ByteOrder() binary.ByteOrder {
	return binary.LittleEndian
}
