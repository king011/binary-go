# binary-go

https://gitlab.com/king011/go-binary 編碼方案的 golang 庫

# 使用方法

1. go get -u -v gitlab.com/king011/binary-go/core
2. 使用 go-binary 生成的 golang 代碼

# Example

```go
package cmd

import (
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/king011/binary-go/core"

	// import 核心實現

	// import go-binary 生成的 代碼
	"gitlab.com/king011/binary-go/protocol/types"
	"gitlab.com/king011/binary-go/protocol/zoo/animal"
)

func init() {
	var cmd = &cobra.Command{
		Use: "example",
		Run: func(cmd *cobra.Command, args []string) {
			// 創建一個 實例
			var t types.Types
			// 初始化 對象 屬性
			{
				t.Int16 = -16                 //基本類型
				t.Ok = true                   // 布爾
				t.Str = "cerberus is an idea" // 字符串
				t.Cat = &animal.Cat{}         // 另外一個對象
				t.Pos = animal.PosCage        // 枚舉

				// 數組
				{
					// 基本類型
					for i := 0; i < 16; i++ {
						t.Int16s = append(t.Int16s, int16(-16+i))
					}
					// bool
					for i := 0; i < 65; i++ {
						t.Oks = append(t.Oks, i%2 != 0)
					}
					// 字符串
					t.Strs = []string{"str0", "", "str1"}
					// 其它對象
					t.Cats = []*animal.Cat{&animal.Cat{}, nil}
					// 枚舉
					t.Poss = []animal.Pos{animal.PosCorridorP0, animal.PosCorridorP1}
				}
			}
			// 返回 默認的 編碼環境
			ctx := core.Background()
			// 編碼數據
			b, e := t.Marshal(ctx)
			if e != nil {
				log.Fatalln(e)
			}
			// 重置對象
			t.Reset()

			// 解碼數據
			e = t.Unmarshal(ctx, b)
			if e != nil {
				log.Fatalln(e)
			}
		},
	}
	rootCmd.AddCommand(cmd)
}
```