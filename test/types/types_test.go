package types_test

import (
	"fmt"
	"testing"

	"gitlab.com/king011/binary-go/core"
	"gitlab.com/king011/binary-go/protocol/types"
	zoo_animal "gitlab.com/king011/binary-go/protocol/zoo/animal"
	test_types "gitlab.com/king011/binary-go/test/types"
)

func dogEqual(d0, d1 *zoo_animal.Dog) (e error) {
	if d0 != nil && d1 != nil {
		if d0.ID != d1.ID {
			e = fmt.Errorf("dog.ID not euqal %v %v", d0.ID, d1.ID)
			return
		}
		if len(d0.Eat) != len(d1.Eat) {
			e = fmt.Errorf("len(d0.Eat)<%v> != len(d1.Eat)<%v>", len(d0.Eat), len(d1.Eat))
			return
		}
		for i := 0; i < len(d0.Eat); i++ {
			if d0.Eat[i] != d1.Eat[i] {
				e = fmt.Errorf("d0.Eat[%v]<%v> != d1.Eat[%v]<%v>", i, d0.Eat[i], i, d1.Eat[i])
				return
			}
		}
	} else {
		if d0 != nil {
			var size int
			size, e = d0.MarshalSize(core.Background(), 0)
			if e != nil {
				return
			}
			if size != 0 {
				e = fmt.Errorf("d0 size<%v> != 0", size)
				return
			}
		}
		if d1 != nil {
			var size int
			size, e = d1.MarshalSize(core.Background(), 0)
			if e != nil {
				return
			}
			if size != 0 {
				e = fmt.Errorf("d1 size<%v> != 0", size)
				return
			}
		}
	}
	return
}
func catEqual(c0, c1 *zoo_animal.Cat) (e error) {
	if c0 != nil && c1 != nil {
		if c0.Name != c1.Name {
			e = fmt.Errorf("Cat.Name not equal <%v> <%v>", c0.Name, c1.Name)
			return
		}
	} else {
		if c0 != nil {
			var size int
			size, e = c0.MarshalSize(core.Background(), 0)
			if e != nil {
				return
			}
			if size != 0 {
				e = fmt.Errorf("c0 size<%v> != 0", size)
				return
			}
		}
		if c1 != nil {
			var size int
			size, e = c1.MarshalSize(core.Background(), 0)
			if e != nil {
				return
			}
			if size != 0 {
				e = fmt.Errorf("c1 size<%v> != 0", size)
				return
			}
		}
	}
	return
}
func typesEqual(t *testing.T, t0, t1 *types.Types) {
	if t0.Int16 != t1.Int16 {
		t.Fatalf("Int16 not equal %v %v", t0.Int16, t1.Int16)
	}
	if t0.Int32 != t1.Int32 {
		t.Fatalf("Int32 not equal %v %v", t0.Int32, t1.Int32)
	}
	if t0.Int64 != t1.Int64 {
		t.Fatalf("Int64 not equal %v %v", t0.Int64, t1.Int64)
	}
	if t0.Uint16 != t1.Uint16 {
		t.Fatalf("Uint16 not equal %v %v", t0.Uint16, t1.Uint16)
	}
	if t0.Uint32 != t1.Uint32 {
		t.Fatalf("Uint32 not equal %v %v", t0.Uint32, t1.Uint32)
	}
	if t0.Uint64 != t1.Uint64 {
		t.Fatalf("Uint64 not equal %v %v", t0.Uint64, t1.Uint64)
	}
	if t0.Float32 != t1.Float32 {
		t.Fatalf("Float32 not equal %v %v", t0.Float32, t1.Float32)
	}
	if t0.Float64 != t1.Float64 {
		t.Fatalf("Float64 not equal %v %v", t0.Float64, t1.Float64)
	}
	if t0.B != t1.B {
		t.Fatalf("B not equal %v %v", t0.B, t1.B)
	}
	if t0.Ok != t1.Ok {
		t.Fatalf("Ok not equal %v %v", t0.Ok, t1.Ok)
	}
	if t0.Str != t1.Str {
		t.Fatalf("Str not equal %v %v", t0.Str, t1.Str)
	}
	e := catEqual(t0.Cat, t1.Cat)
	if e != nil {
		t.Fatal("Cat not equal", e)
	}
	e = dogEqual(t0.Dog, t1.Dog)
	if e != nil {
		t.Fatal("Dog not equal", e)
	}
	if t0.Pos != t1.Pos {
		t.Fatalf("Pos not equal %v %v", t0.Pos, t1.Pos)
	}

	if len(t0.Int16s) != len(t1.Int16s) {
		for i := 0; i < len(t0.Int16s); i++ {
			if t0.Int16s[i] != t1.Int16s[i] {
				t.Fatalf("Int16s[%v] not equal %v %v", i, t0.Int16s[i], t1.Int16s[i])
			}
		}
	}
	if len(t0.Int32s) != len(t1.Int32s) {
		for i := 0; i < len(t0.Int32s); i++ {
			if t0.Int32s[i] != t1.Int32s[i] {
				t.Fatalf("Int32s[%v] not equal %v %v", i, t0.Int32s[i], t1.Int32s[i])
			}
		}
	}
	if len(t0.Int64s) != len(t1.Int64s) {
		for i := 0; i < len(t0.Int64s); i++ {
			if t0.Int64s[i] != t1.Int64s[i] {
				t.Fatalf("Int64s[%v] not equal %v %v", i, t0.Int64s[i], t1.Int64s[i])
			}
		}
	}
	if len(t0.Uint16s) != len(t1.Uint16s) {
		for i := 0; i < len(t0.Uint16s); i++ {
			if t0.Uint16s[i] != t1.Uint16s[i] {
				t.Fatalf("Uint16s[%v] not equal %v %v", i, t0.Uint16s[i], t1.Uint16s[i])
			}
		}
	}
	if len(t0.Uint32s) != len(t1.Uint32s) {
		for i := 0; i < len(t0.Uint32s); i++ {
			if t0.Uint32s[i] != t1.Uint32s[i] {
				t.Fatalf("Uint32s[%v] not equal %v %v", i, t0.Uint32s[i], t1.Uint32s[i])
			}
		}
	}
	if len(t0.Uint64s) != len(t1.Uint64s) {
		for i := 0; i < len(t0.Uint64s); i++ {
			if t0.Uint64s[i] != t1.Uint64s[i] {
				t.Fatalf("Uint64s[%v] not equal %v %v", i, t0.Uint64s[i], t1.Uint64s[i])
			}
		}
	}
	if len(t0.Float32s) != len(t1.Float32s) {
		for i := 0; i < len(t0.Float32s); i++ {
			if t0.Float32s[i] != t1.Float32s[i] {
				t.Fatalf("Float32s[%v] not equal %v %v", i, t0.Float32s[i], t1.Float32s[i])
			}
		}
	}
	if len(t0.Float64s) != len(t1.Float64s) {
		for i := 0; i < len(t0.Float64s); i++ {
			if t0.Float64s[i] != t1.Float64s[i] {
				t.Fatalf("Float64s[%v] not equal %v %v", i, t0.Float64s[i], t1.Float64s[i])
			}
		}
	}
	if len(t0.Bs) != len(t1.Bs) {
		for i := 0; i < len(t0.Bs); i++ {
			if t0.Bs[i] != t1.Bs[i] {
				t.Fatalf("Bs[%v] not equal %v %v", i, t0.Bs[i], t1.Bs[i])
			}
		}
	}
	if len(t0.Oks) != len(t1.Oks) {
		for i := 0; i < len(t0.Oks); i++ {
			if t0.Oks[i] != t1.Oks[i] {
				t.Fatalf("Oks[%v] not equal %v %v", i, t0.Oks[i], t1.Oks[i])
			}
		}
	}
	if len(t0.Strs) != len(t1.Strs) {
		for i := 0; i < len(t0.Strs); i++ {
			if t0.Strs[i] != t1.Strs[i] {
				t.Fatalf("Strs[%v] not equal %v %v", i, t0.Strs[i], t1.Strs[i])
			}
		}
	}
	if len(t0.Cats) != len(t1.Cats) {
		for i := 0; i < len(t0.Cats); i++ {
			if e = catEqual(t0.Cats[i], t1.Cats[i]); e != nil {
				t.Fatalf("Cats[%v] not equal %v", i, e)
			}
		}
	}
	if len(t0.Dogs) != len(t1.Dogs) {
		for i := 0; i < len(t0.Dogs); i++ {
			if e = dogEqual(t0.Dogs[i], t1.Dogs[i]); e != nil {
				t.Fatalf("Dogs[%v] not equal %v", i, e)
			}
		}
	}
	if len(t0.Poss) != len(t1.Poss) {
		for i := 0; i < len(t0.Poss); i++ {
			if t0.Poss[i] != t1.Poss[i] {
				t.Fatalf("Poss[%v] not equal %v %v", i, t0.Poss[i], t1.Poss[i])
			}
		}
	}
}
func TestTypes(t *testing.T) {
	ctx := core.Background()
	var t0, t1 types.Types
	test_types.SetTypes(&t0)
	test_types.SetTypes(&t1)

	size, e := t0.MarshalSize(ctx, 0)
	if e != nil {
		t.Fatal(e)
	}
	data, e := t0.Marshal(ctx)
	if e != nil {
		t.Fatal(e)
	} else if len(data) != size {
		t.Fatalf("len(data)<%v> != size<%v>", len(data), size)
	}

	t0 = types.Types{}
	e = t0.Unmarshal(ctx, data)
	if e != nil {
		t.Fatal(e)
	}
	typesEqual(t, &t0, &t1)
}
