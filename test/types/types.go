package types

import (
	"encoding/json"
	"fmt"
	"log"

	"gitlab.com/king011/binary-go/protocol/types"
	zoo_animal "gitlab.com/king011/binary-go/protocol/zoo/animal"
)

// Display 以 json 打印 測試數據
func Display(v interface{}) {
	data, e := json.MarshalIndent(v, "", "\t")
	if e != nil {
		log.Fatalln(e)
	}
	fmt.Println(string(data))
}

// SetTypes 初始化 測試 數據
func SetTypes(t *types.Types) {
	t.Int16 = -16
	t.Int32 = -32
	t.Int64 = -64
	t.Uint16 = 16
	t.Uint32 = 32
	t.Uint64 = 64
	t.Float32 = 32.32
	t.Float64 = 64.64
	t.B = 8
	t.Ok = true
	t.Str = "cerberus is an idea"
	// cat
	cat := &zoo_animal.Cat{
		Name: "cat",
		Dog: &zoo_animal.Dog{
			ID: 2,
		},
	}
	t.Cat = cat

	// dog
	dog := &zoo_animal.Dog{
		ID:  1,
		Eat: []string{"dog0", "dog1"},
		Cat: &zoo_animal.Cat{
			Name: "dog->cat",
		},
	}
	t.Dog = dog

	t.Pos = zoo_animal.PosCage
	for i := int16(0); i < 16; i++ {
		t.Int16s = append(t.Int16s, -16+i)
	}
	for i := int32(0); i < 16; i++ {
		t.Int32s = append(t.Int32s, -32+i)
	}
	for i := int64(0); i < 16; i++ {
		t.Int64s = append(t.Int64s, -48+i)
	}
	for i := uint16(0); i < 16; i++ {
		t.Uint16s = append(t.Uint16s, 1+i)
	}
	for i := uint32(0); i < 16; i++ {
		t.Uint32s = append(t.Uint32s, 17+i)
	}
	for i := uint64(0); i < 16; i++ {
		t.Uint64s = append(t.Uint64s, 33+i)
	}
	t.Float32s = append(t.Float32s, 32.1)
	t.Float32s = append(t.Float32s, 32.2)

	t.Float64s = append(t.Float64s, 64.1)
	t.Float64s = append(t.Float64s, 64.2)

	for i := byte(0); i < 8; i++ {
		t.Bs = append(t.Bs, i+1)
	}
	for i := 0; i < 65; i++ {
		t.Oks = append(t.Oks, i%2 != 0)
	}
	t.Strs = append(t.Strs, "str0")
	t.Strs = append(t.Strs, "")
	t.Strs = append(t.Strs, "str1")

	t.Cats = append(t.Cats, &zoo_animal.Cat{
		Name: "cat0",
	})
	t.Cats = append(t.Cats, &zoo_animal.Cat{
		Name: "cat1",
	})

	t.Dogs = append(t.Dogs, &zoo_animal.Dog{
		ID:  1,
		Eat: []string{"dog0", "dog1"},
	})
	t.Dogs = append(t.Dogs, nil)
	t.Dogs = append(t.Dogs, &zoo_animal.Dog{
		ID:  2,
		Eat: []string{"dog2", "dog3"},
	})
	t.Dogs = append(t.Dogs, &zoo_animal.Dog{})

	t.Poss = append(t.Poss, zoo_animal.PosCorridorP0)
	t.Poss = append(t.Poss, zoo_animal.PosCorridorP1)
}
