package zoo_test

import (
	"math/rand"
	"testing"
	"time"

	"gitlab.com/king011/binary-go/core"
	zoo_animal "gitlab.com/king011/binary-go/protocol/zoo/animal"
)

func init() {
	rand.Seed(time.Now().Unix())
}
func TestCat(t *testing.T) {
	ctx := core.Background()

	// base
	var cat zoo_animal.Cat
	size, e := cat.MarshalSize(ctx, 0)
	if e != nil {
		t.Fatal(e)
	}
	if size != 0 {
		t.Fatalf("MarshalSize<%v> != 0", size)
	}
	name := "this is cat"
	cat.Name = name

	size, e = cat.MarshalSize(ctx, 0)
	if e != nil {
		t.Fatal(e)
	}
	if size != 4+len(name) {
		t.Fatalf("MarshalSize<%v> != 4 + %v", size, len(name))
	}

	// marshal
	data, e := cat.Marshal(ctx)
	if e != nil {
		t.Fatal(e)
	} else if len(data) != size {
		t.Fatalf("len(data)<%v> != size<%v>", len(data), size)
	}

	// unmarshal
	cat = zoo_animal.Cat{}
	e = cat.Unmarshal(ctx, data)
	if e != nil {
		t.Fatal(e)
	} else if cat.Name != name {
		t.Fatalf("unmarshal name<%v> != name<%v>", cat.Name, name)
	}
}
func dogIsReset(t *testing.T, dog *zoo_animal.Dog) {
	if dog.ID != 0 {
		t.Fatalf("dog.ID<%v> != 0", dog.ID)
	}
	if len(dog.Eat) != 0 {
		t.Fatalf("len(dog.Eat)<%v> != 0", len(dog.Eat))
	}
}
func TestDog(t *testing.T) {
	ctx := core.Background()

	// base
	var dog zoo_animal.Dog
	size, e := dog.MarshalSize(ctx, 0)
	if e != nil {
		t.Fatal(e)
	} else if size != 0 {
		t.Fatalf("MarshalSize<%v> != 0", size)
	}
	dogIsReset(t, &dog)

	// id
	id := rand.Int63() * rand.Int63()
	dog.ID = id
	size, e = dog.MarshalSize(ctx, 0)
	if e != nil {
		t.Fatal(e)
	} else if size != 2+8 {
		t.Fatalf("MarshalSize<%v> != 2+8", size)
	}
	data, e := dog.Marshal(ctx)
	if e != nil {
		t.Fatal(e)
	} else if len(data) != size {
		t.Fatalf("len(data)<%v> != size<%v>", len(data), size)
	}
	dog = zoo_animal.Dog{}
	dogIsReset(t, &dog)

	e = dog.Unmarshal(ctx, data)
	if e != nil {
		t.Fatal(e)
	}
	if dog.ID != id {
		t.Fatalf("dog.ID<%v> != id<%v>", dog.ID, id)
	}
	dog.ID = 0
	dogIsReset(t, &dog)

	// eat
	eat := []string{"eat 0", "eat 1"}
	dog.Eat = eat
	size, e = dog.MarshalSize(ctx, 0)
	if e != nil {
		t.Fatal(e)
	} else if size != 4+2+len(eat[0])+2+len(eat[1]) {
		t.Fatalf("MarshalSize<%v> != 4+2+len(eat[0])+2+len(eat[1])<%v>", size, 4+2+len(eat[0])+2+len(eat[1]))
	}
	data, e = dog.Marshal(ctx)
	if e != nil {
		t.Fatal(e)
	} else if len(data) != size {
		t.Fatalf("len(data)<%v> != size<%v>", len(data), size)
	}

	dog.Eat = nil
	dogIsReset(t, &dog)

	e = dog.Unmarshal(ctx, data)
	if e != nil {
		t.Fatal(e)
	}

	if dog.ID != 0 {
		t.Fatalf("dog.ID<%v> != 0", dog.ID)
	}
	if len(dog.Eat) != len(eat) {
		t.Fatalf("len(dog.Eat)<%v> != len(eat)<%v>", len(dog.Eat), len(eat))
	}
	for i := 0; i < len(eat); i++ {
		if dog.Eat[i] != eat[i] {
			t.Fatalf("dog.Eat[%v]<%v> != eat[%v]<%v>", i, dog.Eat[i], i, eat[i])
		}
	}

	dog.Eat = dog.Eat[:0]
	dogIsReset(t, &dog)

	// id and eat
	id = rand.Int63() * rand.Int63()
	dog.ID = id
	dog.Eat = eat
	size, e = dog.MarshalSize(ctx, 0)
	if e != nil {
		t.Fatal(e)
	} else if size != 2+8+4+2+len(eat[0])+2+len(eat[1]) {
		t.Fatalf("MarshalSize<%v> !=2+8+ 4+2+len(eat[0])+2+len(eat[1])<%v>", size, 2+8+4+2+len(eat[0])+2+len(eat[1]))
	}

	data, e = dog.Marshal(ctx)
	if e != nil {
		t.Fatal(e)
	} else if len(data) != size {
		t.Fatalf("len(data)<%v> != size<%v>", len(data), size)
	}
	dog = zoo_animal.Dog{}
	dogIsReset(t, &dog)
	e = dog.Unmarshal(ctx, data)
	if e != nil {
		t.Fatal(e)
	}

	if dog.ID != id {
		t.Fatalf("dog.ID<%v> != id<%v>", dog.ID, id)
	}
	if len(dog.Eat) != len(eat) {
		t.Fatalf("len(dog.Eat)<%v> != len(eat)<%v>", len(dog.Eat), len(eat))
	}
	for i := 0; i < len(eat); i++ {
		if dog.Eat[i] != eat[i] {
			t.Fatalf("dog.Eat[%v]<%v> != eat[%v]<%v>", i, dog.Eat[i], i, eat[i])
		}
	}
}
