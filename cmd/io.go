package cmd

import (
	"fmt"
	"io/ioutil"
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/king011/binary-go/core"
	"gitlab.com/king011/binary-go/protocol/types"
	test_types "gitlab.com/king011/binary-go/test/types"
)

func init() {
	var w, r string
	var cmd = &cobra.Command{
		Use:   "io",
		Short: "io test",
		Run: func(cmd *cobra.Command, args []string) {
			if w != "" {
				ctx := core.Background()
				var t types.Types
				test_types.SetTypes(&t)
				b, e := t.Marshal(ctx)
				if e != nil {
					log.Fatalln(e)
				}
				e = ioutil.WriteFile(w, b, 0666)
				if e != nil {
					log.Fatalln(e)
				}
				fmt.Println("write to", w)
			} else if r != "" {
				b, e := ioutil.ReadFile(r)
				if e != nil {
					log.Fatalln(e)
				}
				ctx := core.Background()
				var t types.Types
				e = t.Unmarshal(ctx, b)
				if e != nil {
					log.Fatalln(e)
				}
				test_types.Display(&t)
			}
		},
	}
	flags := cmd.Flags()
	flags.StringVarP(&w,
		"write", "w",
		"",
		"write file",
	)
	flags.StringVarP(&r,
		"read", "r",
		"",
		"read file",
	)
	rootCmd.AddCommand(cmd)
}
