package cmd

import (
	"fmt"
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/king011/binary-go/core"
	"gitlab.com/king011/binary-go/protocol/types"
	test_types "gitlab.com/king011/binary-go/test/types"
)

func init() {
	var cmd = &cobra.Command{
		Use:   "types",
		Short: "test types",
		Run: func(cmd *cobra.Command, args []string) {
			var t types.Types
			fmt.Println("***   set   ***")
			test_types.SetTypes(&t)
			test_types.Display(&t)

			fmt.Println("***   marshal   ***")
			ctx := core.Background()
			size, e := t.MarshalSize(ctx, 0)
			if e != nil {
				log.Fatalln(e)
			}
			fmt.Println("size = ", size)
			buffer, e := t.Marshal(ctx)
			if e != nil {
				log.Fatalln(e)
			}
			if len(buffer) != size {
				log.Fatalln("Marshal size not match", size, len(buffer))
			}

			fmt.Println("***   unmarshal   ***")
			t = types.Types{}
			e = t.Unmarshal(ctx, buffer)
			if e != nil {
				log.Fatalln(e)
			}
			test_types.Display(&t)
		},
	}
	rootCmd.AddCommand(cmd)
}
